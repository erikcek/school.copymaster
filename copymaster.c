//#include <CoreServices/CoreServices.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <sys/types.h>
#include <stdlib.h>
#include "options.h"
#include <unistd.h>
#include <time.h>


void FatalError(char c, const char *msg, int exit_status);
void PrintCopymasterOptions(struct CopymasterOptions *cpm_options);
void copyFiles(int input, int output, long long batchSize);
void lseekCopy(int input, int output, long long batchSize, unsigned long num);
bool fileExists(const char *filename);
bool validPermisions(unsigned int permisions);
unsigned short getPermisions(const char *input);
unsigned long long getInode(const char *input);
bool isNormalFile(const char *input);
bool isDirectory(const char *input);
mode_t handleUmask(char umasks[10][4]);

int main(int argc, char *argv[]) {
    struct CopymasterOptions cpm_options = ParseCopymasterOptions(argc, argv);
    
    //-------------------------------------------------------------------
    // Kontrola hodnot prepinacov
    //-------------------------------------------------------------------
    
    // Vypis hodnot prepinacov odstrante z finalnej verzie
    
    PrintCopymasterOptions(&cpm_options);
    
    //-------------------------------------------------------------------
    // Osetrenie prepinacov pred kopirovanim
    //-------------------------------------------------------------------
    
    if (cpm_options.fast && cpm_options.slow) {
        fprintf(stderr, "CHYBA PREPINACOV\n");
        exit(EXIT_FAILURE);
    }
    
    if (cpm_options.create && cpm_options.overwrite) {
        fprintf(stderr, "CHYBA PREPINACOV\n");
        exit(EXIT_FAILURE);
    }
    
    if (cpm_options.create && cpm_options.append) {
        fprintf(stderr, "CHYBA PREPINACOV\n");
        exit(EXIT_FAILURE);
    }
    
    if (cpm_options.overwrite && cpm_options.append) {
        fprintf(stderr, "CHYBA PREPINACOV\n");
        exit(EXIT_FAILURE);
    }
    
    if (cpm_options.overwrite && cpm_options.link) {
        fprintf(stderr, "CHYBA PREPINACOV\n");
        exit(EXIT_FAILURE);
    }
    
    if (cpm_options.append && cpm_options.link) {
        fprintf(stderr, "CHYBA PREPINACOV\n");
        exit(EXIT_FAILURE);
    }
    
    //  if (cpm_options.create && cpm_options.link)
    // {
    //     fprintf(stderr, "CHYBA PREPINACOV\n");
    //     exit(EXIT_FAILURE);
    // }
    
    // TODO Nezabudnut dalsie kontroly kombinacii prepinacov ...
    
    //####################################################################
    
    //-------------------------------------------------------------------
    // Kopirovanie suborov
    //-------------------------------------------------------------------
    
    // TODO Implementovat kopirovanie suborov
    long long batchSize = 20;
    char flag = 'B';
    
    int inputFile = -1;
    DIR *inputDir = NULL;
    int outputFile = -1;
    
    //-------------------------------------------------------------------
    // open / check infile
    //-------------------------------------------------------------------
    
    if (cpm_options.link) {
        if (!fileExists(cpm_options.infile)) {
            FatalError('l', "VSTUPNY SUBOR NEEXISTUJE", 30);
        }
        if (link(cpm_options.infile, cpm_options.outfile) < 0) {
            FatalError('l', "VYSTUPNY SUBOR NEVYTVORENY", 30);
        }
    }
    
    if (!fileExists(cpm_options.infile)) {
        FatalError('B', "SUBOR NEEXISTUJE", 21);
    }
    if (cpm_options.inode) {
        unsigned long long inode = getInode(cpm_options.infile);
        if (inode != cpm_options.inode_number) {
            FatalError('i', "ZLY INODE", 27);
        }
    }
    if (cpm_options.directory) {
        if (!isDirectory(cpm_options.infile)) {
            FatalError('D', "VSTUPNY SUBOR NIE JE ADRESAR", 28);
        }
        inputDir = opendir(cpm_options.infile);
        if (inputDir == NULL) {
            FatalError('D', "INA CHYBA", 28);
        }
    } else {
        if (!isNormalFile(cpm_options.infile)) {
            FatalError('B', "INA CHYBA", 23);
        }
        inputFile = open(cpm_options.infile, O_RDONLY);
    }
    if (inputFile == -1 && inputDir == NULL) {
        FatalError('B', "INA CHYBA", 23);
    }
    
    //-------------------------------------------------------------------
    // open / check outfile
    //-------------------------------------------------------------------
    
    if (cpm_options.umask) {
        umask(handleUmask(cpm_options.umask_options));
    }
    
    if (cpm_options.create) {
        if (fileExists(cpm_options.outfile)) {
            //errno = EEXIST;
            FatalError('c', "SUBOR EXISTUJE", 23);
        }
        if (!validPermisions(cpm_options.create_mode)) {
            FatalError('c', "ZLE PRAVA", 23);
        }
        outputFile = open(cpm_options.outfile, O_CREAT | O_EXCL | O_WRONLY,
                          cpm_options.create_mode);
    }
    
    else if (cpm_options.overwrite) {
        if (!fileExists(cpm_options.outfile)) {
            //errno = 2;
            FatalError('o', "SUBOR NEEXISTUJE", 24);
        }
        outputFile = open(cpm_options.outfile, O_WRONLY | O_TRUNC);
        if (outputFile == -1) {
            FatalError('c', "INA CHYBA", 24);
        }
    }
    
    else if (cpm_options.append) {
        if (!fileExists(cpm_options.outfile)) {
            //errno = 2;
            FatalError('o', "SUBOR NEEXISTUJE", 22);
        }
        outputFile = open(cpm_options.outfile, O_WRONLY | O_APPEND);
        if (outputFile == -1) {
            FatalError('c', "INA CHYBA", 22);
        }
    } else {
        unsigned short perm = getPermisions(cpm_options.infile);
        if (cpm_options.lseek) {
            outputFile = open(cpm_options.outfile, O_WRONLY | O_CREAT, perm);
        }
        else {
            outputFile = open(cpm_options.outfile, O_WRONLY | O_CREAT | O_TRUNC, perm);
        }
        if (outputFile == -1) {
            FatalError('c', "INA CHYBA", 21);
        }
    }
    
    if (outputFile == -1) {
        FatalError('c', "INA CHYBA", 21);
    }
    
    //-------------------------------------------------------------------
    // Vypis adresara
    //-------------------------------------------------------------------
    
    if (cpm_options.directory) {
        struct dirent *temp;
        char *buffer = malloc(sizeof(char) * 40);
        while ((temp = readdir(inputDir)) != NULL) {  // Citame kym je co citat
            struct stat t_stat;
            stat(temp->d_name, &t_stat);
            
            time_t t = t_stat.st_ctime;
            time_t lt;
            localtime_r(&t, (struct tm*)&lt);
            char timbuf[80];
            strftime(timbuf, sizeof(timbuf), "%B %d %Y / %I:%M %p", (const struct tm*)&lt);
            printf("%d %d %lld %d %s %s\n", t_stat.st_uid, t_stat.st_gid, t_stat.st_size, t_stat.st_nlink, timbuf, temp->d_name);
            
            sprintf(buffer, "%d %d %lld %d %s %s\n", t_stat.st_uid, t_stat.st_gid, t_stat.st_size, t_stat.st_nlink, timbuf, temp->d_name);
            printf("%s", buffer);
            ssize_t temp = strlen(buffer);
            write(outputFile, buffer, temp);
            
        }
        free(buffer);
        closedir(inputDir);
        exit(0);
    }
    
    if (cpm_options.sparse) {
        struct stat tempBuffer;
        if (lstat(cpm_options.infile, &tempBuffer) < 0) {
            FatalError('s', "RIEDKY SUBOR NEVYTVORENY", 41);
        }
        //char *buffer = malloc(sizeof(char) * tempBuffer.st_blksize);
        //int size = tempBuffer.st_blksize;
        char buffer;
        int size = tempBuffer.st_size;
        ftruncate(outputFile, size);
        ssize_t temp;
        while ((temp = read(inputFile, &buffer, 1)) != 0) {
//            printf("%d", buffer == '\0' ? 1 : 0);
//            printf("%c\n", buffer);
            if (buffer == '\0') {
                lseek(outputFile, 1, SEEK_CUR);
            }
            else {
//                printf("%c ", buffer);
                write(outputFile, &buffer, 1);
            }
        }
        exit(0);
        //free(buffer);
    }
    
    //-------------------------------------------------------------------
    // slow / fast
    //-------------------------------------------------------------------
    
    if (cpm_options.slow) {
        batchSize = 1;
        flag = 's';
    }
    
    if (cpm_options.fast) {
        struct stat tempBuffer;
        if (lstat(cpm_options.infile, &tempBuffer) < 0) {
            FatalError('f', "INÁ CHYBA", 1);
        }
        batchSize = tempBuffer.st_size;
        flag = 'f';
    }
    
    //-------------------------------------------------------------------
    // lseek
    //-------------------------------------------------------------------
    
    if (cpm_options.lseek) {
        off_t iPos = lseek(inputFile, cpm_options.lseek_options.pos1, SEEK_SET);
        if (iPos < 0) {
            FatalError('l', "CHYBA POZICIE infile", 33);
        }
        off_t oPos = lseek(outputFile, cpm_options.lseek_options.pos2,
                           cpm_options.lseek_options.x);
        if (!oPos) {
            FatalError('l', "CHYBA POZICIE outfile", 33);
        }
        if (!cpm_options.slow) {
            batchSize = cpm_options.lseek_options.num;
        }
        lseekCopy(inputFile, outputFile, batchSize,
                  cpm_options.lseek_options.num);
        
        //-------------------------------------------------------------------
        // copy files
        //-------------------------------------------------------------------
        
    } else {
        copyFiles(inputFile, outputFile, batchSize);
    }
    
    //-------------------------------------------------------------------
    // Osetrenie prepinacov po kopirovani
    //-------------------------------------------------------------------
    
    // TODO Implementovat osetrenie prepinacov po kopirovani
    
    if (cpm_options.chmod) {
        if (!validPermisions(cpm_options.chmod_mode)) {
            FatalError('m', "ZLE PRAVA", 26);
        }
        if (chmod(cpm_options.outfile, cpm_options.chmod_mode) == -1) {
            FatalError('m', "INA CHYBA", 26);
        }
    }
    
    if (cpm_options.truncate) {
        if (truncate(cpm_options.infile, cpm_options.truncate_size) == -1) {
            FatalError('t', "VSTUPNY SUBOR NEZMENENY", 31);
        }
    }
    
    if (cpm_options.delete_opt) {
        if (unlink(cpm_options.infile) != 0) {
            FatalError('d', " SUBOR NEBOL ZMAZANY", 26);
        }
    }
    
    close(inputFile);
    close(outputFile);
    
    return 0;
}

void FatalError(char c, const char *msg, int exit_status) {
    fprintf(stderr, "%c:%d\n", c, errno);
    fprintf(stderr, "%c:%s\n", c, strerror(errno));
    fprintf(stderr, "%c:%s\n", c, msg);
    fprintf(stderr, "%d\n", exit_status);
    exit(exit_status);
}

void PrintCopymasterOptions(struct CopymasterOptions *cpm_options) {
    if (cpm_options == 0) return;
    
    printf("infile:        %s\n", cpm_options->infile);
    printf("outfile:       %s\n", cpm_options->outfile);
    
    printf("fast:          %d\n", cpm_options->fast);
    printf("slow:          %d\n", cpm_options->slow);
    printf("create:        %d\n", cpm_options->create);
    printf("create_mode:   %o\n", (unsigned int)cpm_options->create_mode);
    printf("overwrite:     %d\n", cpm_options->overwrite);
    printf("append:        %d\n", cpm_options->append);
    printf("lseek:         %d\n", cpm_options->lseek);
    
    printf("lseek_options.x:    %d\n", cpm_options->lseek_options.x);
    printf("lseek_options.pos1: %lld\n", cpm_options->lseek_options.pos1);
    printf("lseek_options.pos2: %lld\n", cpm_options->lseek_options.pos2);
    printf("lseek_options.num:  %lu\n", cpm_options->lseek_options.num);
    
    printf("directory:     %d\n", cpm_options->directory);
    printf("delete_opt:    %d\n", cpm_options->delete_opt);
    printf("chmod:         %d\n", cpm_options->chmod);
    printf("chmod_mode:    %o\n", (unsigned int)cpm_options->chmod_mode);
    printf("inode:         %d\n", cpm_options->inode);
    printf("inode_number:  %llu\n", cpm_options->inode_number);
    
    printf("umask:\t%d\n", cpm_options->umask);
    for (unsigned int i = 0; i < kUMASK_OPTIONS_MAX_SZ; ++i) {
        if (cpm_options->umask_options[i][0] == 0) {
            // dosli sme na koniec zoznamu nastaveni umask
            break;
        }
        printf("umask_options[%u]: %s\n", i, cpm_options->umask_options[i]);
    }
    
    printf("link:          %d\n", cpm_options->link);
    printf("truncate:      %d\n", cpm_options->truncate);
    printf("truncate_size: %lld\n", cpm_options->truncate_size);
    printf("sparse:        %d\n", cpm_options->sparse);
}

void copyFiles(int input, int output, long long batchSize) {
    char *buffer = malloc(sizeof(char) * batchSize);
    ssize_t temp = 0;
    while ((temp = read(input, buffer, (ssize_t)batchSize)) != 0) {
        write(output, buffer, temp);
    }
    free(buffer);
}

void lseekCopy(int input, int output, long long batchSize, unsigned long num) {
    char *buffer = malloc(sizeof(char) * batchSize);
    ssize_t temp = 0;
    unsigned int counter = 0;
    while ((temp = read(input, buffer, (ssize_t)batchSize)) != 0 &&
           counter < num) {
        write(output, buffer, temp);
        counter += temp;
    }
    free(buffer);
}

bool fileExists(const char *filename) { return (access(filename, F_OK) == 0); }

bool validPermisions(unsigned int permisions) {
    if (permisions == 0) {
        return false;
    }
    return true;
}

unsigned short getPermisions(const char *input) {
    struct stat s;
    lstat(input, &s);
    return s.st_mode;
}

unsigned long long getInode(const char *input) {
    struct stat s;
    lstat(input, &s);
    return s.st_ino;
}

bool isNormalFile(const char *input) {
    struct stat s;
    lstat(input, &s);
    return S_ISREG(s.st_mode);
}

bool isDirectory(const char *input) {
    struct stat s;
    lstat(input, &s);
    return S_ISDIR(s.st_mode);
}

mode_t handleUmask(char umasks[10][4]) {
    mode_t mask = umask(0);
    for (int i = 0; i < 10; i++) {
        switch (umasks[i][0]) {
            case 'u':
                if (umasks[i][1] == '-') {
                    switch (umasks[i][2]) {
                        case 'r':
                            mask = mask | S_IRUSR;
                            break;
                        case 'w':
                            mask = mask | S_IWUSR;
                            break;
                        case 'x':
                            mask = mask | S_IXUSR;
                            break;
                        default:
                            break;
                    }
                } else if (umasks[i][1] == '+') {
                    switch (umasks[i][2]) {
                        case 'r':
                            mask = (mask | S_IRUSR) ^ S_IRUSR;
                            break;
                        case 'w':
                            mask = (mask | S_IWUSR) ^ S_IWUSR;
                            break;
                        case 'x':
                            mask = (mask | S_IXUSR) ^ S_IXUSR;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 'g':
                if (umasks[i][1] == '-') {
                    switch (umasks[i][2]) {
                        case 'r':
                            mask = mask | S_IRGRP;
                            break;
                        case 'w':
                            mask = mask | S_IWGRP;
                            break;
                        case 'x':
                            mask = mask | S_IWGRP;
                            break;
                        default:
                            break;
                    }
                } else if (umasks[i][1] == '+') {
                    switch (umasks[i][2]) {
                        case 'r':
                            mask = (mask | S_IRGRP) ^ S_IRGRP;
                            break;
                        case 'w':
                            mask = (mask | S_IWGRP) ^ S_IWGRP;
                            break;
                        case 'x':
                            mask = (mask | S_IWGRP) ^ S_IWGRP;
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 'o':
                if (umasks[i][1] == '-') {
                    switch (umasks[i][2]) {
                        case 'r':
                            mask = mask | S_IROTH;
                            break;
                        case 'w':
                            mask = mask | S_IROTH;
                            break;
                        case 'x':
                            mask = mask | S_IROTH;
                            break;
                        default:
                            break;
                    }
                } else if (umasks[i][1] == '+') {
                    switch (umasks[i][2]) {
                        case 'r':
                            mask = (mask | S_IROTH) ^ S_IROTH;
                            break;
                        case 'w':
                            mask = (mask | S_IROTH) ^ S_IROTH;
                            break;
                        case 'x':
                            mask = (mask | S_IROTH) ^ S_IROTH;
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }
    return mask;
}
